package ro.siit.secretsanta.dto;

import ro.siit.secretsanta.model.User;

import java.time.LocalDate;
import java.util.List;

public class GroupDTO {
    private String groupName;
    List<User> userList;
    private LocalDate giftExchangeDate;
    private int budget;

    public GroupDTO() {
    }

    public LocalDate getGiftExchangeDate() {
        return giftExchangeDate;
    }

    public void setGiftExchangeDate(LocalDate giftExchangeDate) {
        this.giftExchangeDate = giftExchangeDate;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public GroupDTO(String groupName, List<User> userList, LocalDate giftExchangeDate, int budget) {
        this.groupName = groupName;
        this.userList = userList;
        this.giftExchangeDate = giftExchangeDate;
        this.budget = budget;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
