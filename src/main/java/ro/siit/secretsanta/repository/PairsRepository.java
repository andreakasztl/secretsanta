package ro.siit.secretsanta.repository;

import org.springframework.data.repository.CrudRepository;
import ro.siit.secretsanta.model.Pairs;

public interface PairsRepository extends CrudRepository<Pairs,Integer> {
}
