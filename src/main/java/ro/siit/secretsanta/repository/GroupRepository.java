package ro.siit.secretsanta.repository;

import org.springframework.data.repository.CrudRepository;
import ro.siit.secretsanta.model.Group;

import java.util.Optional;

public interface GroupRepository extends CrudRepository<Group,Integer> {

}
