package ro.siit.secretsanta.service;

import org.springframework.stereotype.Service;
import ro.siit.secretsanta.dto.GroupDTO;
import ro.siit.secretsanta.model.Group;
import ro.siit.secretsanta.model.Pairs;
import ro.siit.secretsanta.model.User;
import ro.siit.secretsanta.repository.GroupRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class GroupService {
    final
    GroupRepository groupRepository;
    final
    UserService userService;
    final
    EmailService emailService;
    final
    SecretSantaGeneratorService secretSantaGeneratorService;

    public GroupService(GroupRepository groupRepository, UserService userService, EmailService emailService, SecretSantaGeneratorService secretSantaGeneratorService) {
        this.groupRepository = groupRepository;
        this.userService = userService;
        this.emailService = emailService;
        this.secretSantaGeneratorService = secretSantaGeneratorService;
    }

    public Group createEmptyGroup(Group group){
        return groupRepository.save(group);
    }

    @Transactional(value = Transactional.TxType.REQUIRED,rollbackOn = Exception.class)
    public Group createGroupWithUsers(GroupDTO groupDTO){
        List<User> userList = new ArrayList<>();
        for (User user : groupDTO.getUserList()){
            userList.add(userService.createUser(user.getName(),user.getEmail()));
        }

        Group group = new Group();
        group.setUserList(userList);
        group.setGroupName(groupDTO.getGroupName());
        group.setBudget(groupDTO.getBudget());
        group.setGiftExchangeDate(groupDTO.getGiftExchangeDate());

        return  groupRepository.save(group);

    }

    public Group generatePairs(int groupId){
        if (!groupRepository.findById(groupId).isPresent()){
            throw new IllegalArgumentException("Group does not exist.");
        }
        Group group = groupRepository.findById(groupId).get();

        group.emptyPairs();
        groupRepository.save(group);
        group.addAllPairs(secretSantaGeneratorService.generatePairs(group.getUserList()));
        groupRepository.save(group);
        sendSecretSantaNotification(group);

        return group;

    }

    public void sendSecretSantaNotification(Group group){
        for( Pairs p : group.getPairList()){
            emailService.sendGifteeNotification(p.getGifter(),p.getGiftee(),group);
        }
    }

    public void sendSecretSantaNotification(int groupId){
        final Group group;
        if (!groupRepository.findById(groupId).isPresent())
            throw new IllegalArgumentException("Group does not exist.");
        group = groupRepository.findById(groupId).get();

        for( Pairs p : group.getPairList()){
            emailService.sendGifteeNotification(p.getGifter(),p.getGiftee(),group);
        }
    }
//    @PostConstruct
//    public void addDummyUsers(){
//        User testUser1 = userService.createUser("test1", "test1@email.com");
//        User testUser2 = userService.createUser("test2", "test2@email.com");
//        Group testGroup = new Group();
//        testGroup.setGroupName("iskolasok");
//        testGroup.setBudget(100);
//        testGroup.setGiftExchangeDate(LocalDate.now());
//        testGroup.addUser(testUser1);
//        testGroup.addUser(testUser2);
//        emailService.sendGifteeNotification(testUser1,testUser2,testGroup);
//
//
//    }
}
