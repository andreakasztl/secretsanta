package ro.siit.secretsanta.service;

import org.springframework.stereotype.Service;
import ro.siit.secretsanta.model.Pairs;
import ro.siit.secretsanta.model.User;
import ro.siit.secretsanta.repository.PairsRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class SecretSantaGeneratorService {
    final PairsRepository pairsRepository;

    public SecretSantaGeneratorService(PairsRepository pairsRepository) {
        this.pairsRepository = pairsRepository;
    }

    public List<Pairs> generatePairs(List<User> userList){
        List<User> gifters = new ArrayList<>();
        List<User> giftees = new ArrayList<>();
        gifters.addAll(userList);
        giftees.addAll(userList);

        boolean isOk = false;
        while(!isOk){
            Collections.shuffle(giftees);
            Collections.shuffle(gifters);
            isOk = true;
            for (int i =0; i<userList.size();i++){
                if (giftees.get(i).getEmail().equals(gifters.get(i).getEmail())) {
                    isOk = false;
                    break;
                }
            }
        }



        List<Pairs> pairs = new ArrayList<>();
        for (int i =0; i<userList.size();i++){
            Pairs pair = new Pairs();
            pair.setGiftee(giftees.get(i));
            pair.setGifter(gifters.get(i));
            pairsRepository.save(pair);
            pairs.add(pair);
        }
        return pairs;
    }

}
