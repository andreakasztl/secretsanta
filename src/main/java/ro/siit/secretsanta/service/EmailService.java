package ro.siit.secretsanta.service;

import com.sun.mail.smtp.SMTPSendFailedException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import ro.siit.secretsanta.model.Group;
import ro.siit.secretsanta.model.User;

import java.util.concurrent.TimeUnit;

@Service
public class EmailService {


    private final JavaMailSender emailSender;

    public EmailService(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Async("threadPoolTaskExecutor")
    public void sendGifteeNotification(
            User to, User giftee, Group group) {
        try{
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("noreply@secretsanta.com");
        message.setTo(to.getEmail());
        message.setSubject("Make this.one happy!");
        message.setText("Dear Secret Santa!"+ "\n" + "\n" + "You've been selected to bring a little joy to " + giftee.getName() + " on this special day of " + group.getGiftExchangeDate() + "!" + "\n" + "\n" + "Choose the best gift, which doesn't exceed the budget ( " + group.getBudget() + " ) !" + "\n" + "\n" + "Happy holiday!" );

        emailSender.send(message);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }finally {
            System.out.println("Email sent to: " + to.getName());
        }

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}