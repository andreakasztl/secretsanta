package ro.siit.secretsanta.service;

import org.springframework.stereotype.Service;
import ro.siit.secretsanta.model.User;
import ro.siit.secretsanta.repository.UserRepository;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Service
public class UserService {
    final
    UserRepository userRepository;
    final
    EmailService emailService;


    public UserService(UserRepository userRepository, EmailService emailService) {
        this.userRepository = userRepository;
        this.emailService = emailService;
    }

    public boolean isEmailValid(String email){
        if (email == null)
            return false;
        Pattern p = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        return p.matcher(email).matches();
       // return email.matches();
    }
    public User createUser(String name, String email){
        if (isEmailValid(email)) {
            User user = new User(email, name);
            return userRepository.save(user);
        }
        else throw new IllegalArgumentException("Invalid email address.");
    }

    public List<User> getALlUsers(){
        List<User> copy = new ArrayList<>();
        copy.addAll(userRepository.findAll());

        return copy;
    }

    public List<User> createUsers(List<User> userList) {
        return (List<User>) userRepository.saveAll(userList);
    }

//    @PostConstruct
//    public void addDummyUsers(){
//        User testUser1 = new User("test1@email.com","test1");
//        User testUser2 = new User("test2@email.com","test2");
//        userRepository.save(testUser1);
//        userRepository.save(testUser2);
//        emailService.sendGifteeNotification(testUser1,testUser2);
//
//    }
}
