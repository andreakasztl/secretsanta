package ro.siit.secretsanta.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ro.siit.secretsanta.dto.GroupDTO;
import ro.siit.secretsanta.model.Group;
import ro.siit.secretsanta.service.GroupService;

@RestController
@RequestMapping("/groups")
public class GroupController {
    final
    GroupService groupService;

    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @PostMapping
    public Group createEmptyGroup(@RequestBody Group group){
        return groupService.createEmptyGroup(group);
    }

    @PostMapping(path="/withusers")
    public Group createGroupWithUsers(@RequestBody GroupDTO groupDTO){
        try {
            return groupService.createGroupWithUsers(groupDTO);
        }
        catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,e.getMessage(),e);
        }
    }

    @PatchMapping(path = "/generate/{groupId}")
    public Group generatePairs(@PathVariable int groupId){
        try {
            return groupService.generatePairs(groupId);

        }catch(IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @GetMapping(path = "/notify/{groupId}")
    public void notifySecretSanta(@PathVariable int groupId){
        try {
            groupService.sendSecretSantaNotification(groupId);
        }catch(IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

  }
