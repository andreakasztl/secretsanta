package ro.siit.secretsanta.model;

import javax.persistence.*;


@Entity
@Table(name = "pairs")
public class Pairs {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne
    public User gifter;

    @OneToOne
    public User giftee;

    public Pairs(){

    }

    public Pairs(int id, User gifter, User giftee){
        this.id = id;
        this.gifter = gifter;
        this.giftee = giftee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getGifter() {
        return gifter;
    }

    public void setGifter(User gifter) {
        this.gifter = gifter;
    }

    public User getGiftee() {
        return giftee;
    }

    public void setGiftee(User giftee) {
        this.giftee = giftee;
    }

    //   @OneToOne
//    Map<User,User> pairMappings = new HashMap<>();
//
//    public Pairs(){
//
//    };
//
//    public Map<User, User> getPairMappings() {
//        return pairMappings;
//    }
//
//    public void addPairMapping(User gifter, User giftee){
//       pairMappings.put(gifter,giftee);
//    }
}
