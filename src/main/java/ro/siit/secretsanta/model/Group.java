package ro.siit.secretsanta.model;



import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "groups")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String groupName;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Pairs> pairList;

    @OneToMany
    private List<User> userList;

    private LocalDate giftExchangeDate;

    private int budget;

    public LocalDate getGiftExchangeDate() {
        return giftExchangeDate;
    }

    public void setGiftExchangeDate(LocalDate giftExchangeDate) {
        this.giftExchangeDate = giftExchangeDate;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public Group(String groupName, List<User> userList, LocalDate giftExchangeDate, int budget) {
        this.groupName = groupName;
        this.userList = userList;
        this.giftExchangeDate = giftExchangeDate;
        this.budget = budget;
        this.pairList = new ArrayList<>();
    }

    public Group (int id, String groupName){
        this.id = id;
        this.groupName = groupName;
        pairList = new ArrayList<>();
        userList = new ArrayList<>();
    }

    public Group() {

        pairList = new ArrayList<>();
        userList = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<Pairs> getPairList() {
        return pairList;
    }

    public void addPair(Pairs pair){
        this.pairList.add(pair);
    }

    public List<User> getUserList() {
        return userList;
    }

    public void addUser(User user){
        this.userList.add(user);
    }

    public void setUserList(List<User> userList) {
        this.userList.addAll(userList);
    }

    public void addAllPairs(List<Pairs> generatedPairs){
        this.pairList.addAll(generatedPairs);
    }

    public void emptyPairs(){
        this.pairList.clear();
    }
}
