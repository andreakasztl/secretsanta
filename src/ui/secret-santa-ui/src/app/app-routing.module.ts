import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from "./main/main.component";
import {GeneratorComponent} from "./generator/generator.component";



const routes: Routes = [
  {path: 'home', component: MainComponent},
  {path: 'generated', component: GeneratorComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
