export class Participant{
  name: string;
  email: string;

  constructor(name, email) {
    this.name = name;
    this.email = email;
  }

}
