import {Participant} from "./participant";
import {Pair} from "./Pair";

export class Group{
  id: number;
  groupName: string;
  userList: Participant[];
  giftExchangeDate: Date;
  budget:number;
  pairList:Array<Pair>;


  constructor(id?: number, groupName?: string, userList?: Participant[], giftExchangeDate?: Date, budget?: number,pairList?: Array<Pair>) {
    this.id = id;
    this.groupName = groupName;
    this.userList = userList;
    this.giftExchangeDate = giftExchangeDate;
    this.budget = budget;
    this.pairList = pairList;
  }


}
