import {Participant} from "./participant";

export class Pair{
  gifter: Participant;
  giftee: Participant;

  constructor(gifter: Participant, giftee: Participant) {
    this.giftee = giftee;
    this.gifter = gifter;
  }
}
