import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {GeneratorService} from "../service/generator.service";
import {Group} from "../entities/group";
import {Subject} from "rxjs";

@Component({
  selector: 'app-generator',
  templateUrl: './generator.component.html',
  styleUrls: ['./generator.component.scss']
})
export class GeneratorComponent implements OnInit {

  private groupId : number;
  group: Group;
  private done: Subject<boolean>;
  error: string;
  success: boolean;
  constructor(private router:Router, private generatorService: GeneratorService) { }

  ngOnInit(): void {
    this.done = new Subject();
    this.groupId = parseInt(sessionStorage.getItem('groupId'));
    if (this.groupId){
      this.generatorService.generatePairs(this.groupId).subscribe(
        group => {
          this.group = group;
          console.log(group);
          this.done.next(true);
          this.success = true;
        },
        error => {
          this.error = error.error.message;
          this.success = false;
        }
      )
      sessionStorage['groupId'] = null;
    }
    else{
      alert("You have to fill in the form before accessing this page.");
      this.router.navigateByUrl('/home');
    }

  }

  get groupText(){
    let txt = '';

    this.group?.userList.forEach(
      u => {
        txt = txt + u.name + ', '
      }
    )
    txt = txt.substring(0,txt.length-2);

    return txt;

  }

}
