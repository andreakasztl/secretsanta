import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Participant} from "../entities/participant";
import {Group} from "../entities/group";
import {GroupService} from "../service/group.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-creator',
  templateUrl: './creator.component.html',
  styleUrls: ['./creator.component.scss']
})
export class CreatorComponent implements OnInit {

  nameFormGroup: FormGroup;
  participantFormGroup: FormGroup;
  detailsFormGroup: FormGroup;
  numberOfParticipants: number;
  budget: number;
  group: Group;



  constructor(private _formBuilder: FormBuilder, private groupService: GroupService, private router:Router) {}

  ngOnInit() {
    this.nameFormGroup = this._formBuilder.group({
      nameCtrl: ['', Validators.required],
      numCtrl: ['',[Validators.min(3),Validators.pattern("\\d+")]]
    });


    this.participantFormGroup = this._formBuilder.group({
      nameArray: this._formBuilder.array([
        this._formBuilder.control('',[Validators.required,Validators.minLength(3)])
      ]),
      emailArray: this._formBuilder.array([
        this._formBuilder.control('',[Validators.required,Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\\\.[a-z]{2,4}$')])
      ])
    });
    this.detailsFormGroup = this._formBuilder.group({
      date: this._formBuilder.control(''),
      budget: this._formBuilder.control('',Validators.min(1)),
      message: this._formBuilder.control('')
    });

    this.group = new Group();


  }

  setNumberOfParticipant(){
    this.numberOfParticipants = this.nameFormGroup.controls['numCtrl'].value;

    this.emails.clear();
    this.names.clear();
    for (let i=0; i<this.numberOfParticipants; i++){
      this.addEmail();
      this.addName();
    }
    this.group.userList = new Array(this.numberOfParticipants);


  }

  get names() {
    return this.participantFormGroup.get('nameArray') as FormArray;
  }
  get emails() {
    return this.participantFormGroup.get('emailArray') as FormArray;
  }
  addName() {
    this.names.push(this._formBuilder.control('',Validators.required));
  }
  addEmail() {
    this.emails.push(this._formBuilder.control('',[Validators.required,Validators.email]));
  }

  createGroup(){
    this.group.groupName = this.nameFormGroup.controls['nameCtrl'].value;
    let participants:Participant[] = new Array(this.numberOfParticipants);
    for (let i=0;i<this.numberOfParticipants;i++){
      let p:Participant = new Participant(this.names.at(i).value, this.emails.at(i).value);
      participants[i] = p;
    }
    this.group.userList = participants;
    this.group.budget = this.detailsFormGroup.controls['budget'].value;
    this.group.giftExchangeDate = this.detailsFormGroup.controls['date'].value;
    this.groupService.createGroup(this.group).subscribe(
      gr => {
        this.group = gr;
        sessionStorage.setItem('groupId',this.group.id.toString());
        console.log('Group id saved to session: ' + sessionStorage.getItem('groupId'));
        this.router.navigateByUrl('/generated');
      },
      error => {
        alert(error.error.message);
        console.log(error);
      }
    );

  }
}
