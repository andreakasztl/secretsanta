import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Group} from "../entities/group";

@Injectable({
  providedIn: 'root'
})
export class GeneratorService {

  constructor(private http: HttpClient) { }

  public generatePairs (groupId: number){
    return this.http.patch<Group>('/api/groups/generate/'+groupId,undefined)
  }

}
